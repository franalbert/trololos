package com.example.cereon.trololos;

import android.preference.PreferenceActivity;
import android.os.Bundle;

public class preferences extends PreferenceActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_preferences);
        addPreferencesFromResource(R.xml.preferences);
    }
}
