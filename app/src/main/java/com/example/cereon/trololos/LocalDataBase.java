package com.example.cereon.trololos;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class LocalDataBase extends SQLiteOpenHelper {

    String sqlCreate = "CREATE TABLE MyMemes (id INTEGER PRIMARY KEY, route TEXT)";
    String sqlDrop = "DROP TABLE IF EXISTS Mymemes";
    /*
    * The AUTOINCREMENT keyword imposes extra CPU, memory, disk space, and disk I/O overhead and should
    * be avoided if not strictly needed. It is usually not needed.
    *
    * In SQLite, a column with type INTEGER PRIMARY KEY is an alias for the ROWID (except in WITHOUT ROWID tables)
    * which is always a 64-bit signed integer.
    *
    * On an INSERT, if the ROWID or INTEGER PRIMARY KEY column is not explicitly given a value, then it will
    * be filled automatically with an unused integer, usually one more than the largest ROWID currently in use.
    * This is true regardless of whether or not the AUTOINCREMENT keyword is used.
    *
    * If the AUTOINCREMENT keyword appears after INTEGER PRIMARY KEY, that changes the automatic ROWID assignment
    * algorithm to prevent the reuse of ROWIDs over the lifetime of the database. In other words, the purpose of
    * AUTOINCREMENT is to prevent the reuse of ROWIDs from previously deleted rows.
    * */
    public LocalDataBase(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }
    //crea la bd la primera vez que se cree un objeto LocalDataBase
    @Override
    public void onCreate(SQLiteDatabase db) {
        //creacion de tablas e inserts iniciales
        db.execSQL(sqlCreate);
        for(int i=1; i<=10; i++)
        {
            db.execSQL("INSERT INTO MyMemes (id,route) VALUES (i,'testlink" + i + "')");
        }
    }
    //Se ejecuta al actulizar la version de la bd actualmente reinicia la bd
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(sqlDrop);
        db.execSQL(sqlCreate);
        for(int i=1; i<=10; i++)
        {
            db.execSQL("INSERT INTO MyMemes (id,route) VALUES (i,'testlink" + i + "')");
        }
    }
}
